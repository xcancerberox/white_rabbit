import unittest

from whiterabbit.activity_tracker import event_model


class TestEvent(unittest.TestCase):
    def test_default_start(self):
        event = event_model.Event('foo')
        self.assertEqual(event.status, event_model.STATUS_STARTED)

    def test_stop_event(self):
        event = event_model.Event('foo')
        event.stop()
        self.assertEqual(event.status, event_model.STATUS_STOPED)

    def test_eq(self):
        event = event_model.Event('foo')
        self.assertEqual(event, event)

    def test_neq_by_name(self):
        event1 = event_model.Event('foo')
        event2 = event_model.Event.loadd(event1.dumpd())
        event2.name = 'bar'
        self.assertNotEqual(event1, event2)

    def test_neq_by_status(self):
        event1 = event_model.Event('foo')
        event2 = event_model.Event.loadd(event1.dumpd())
        event2.status = event_model.STATUS_STOPED
        self.assertNotEqual(event1, event2)

    def test_neq_by_sart_time(self):
        event1 = event_model.Event('foo')
        event2 = event_model.Event.loadd(event1.dumpd())
        import time
        import datetime
        time.sleep(0.1)
        event2.start_time = datetime.datetime.now()
        self.assertNotEqual(event1, event2)

    def test_neq_by_stop_time(self):
        event1 = event_model.Event('foo')
        event2 = event_model.Event.loadd(event1.dumpd())
        event1.stop()
        import time
        time.sleep(0.1)
        event2.stop()
        self.assertNotEqual(event1, event2)

    def test_neq_by_id(self):
        event1 = event_model.Event('foo')
        event2 = event_model.Event('foo')
        event2.start_time = event1.start_time
        self.assertNotEqual(event1, event2)

    def test_loadd_dumpd(self):
        event = event_model.Event('foo')
        new_event = event_model.Event.loadd(event.dumpd())
        self.assertEqual(event, new_event)

    def test_name_is_string(self):
        with self.assertRaises(TypeError):
            event_model.Event(None)
