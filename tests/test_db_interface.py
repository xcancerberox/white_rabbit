import unittest
import tempfile

from whiterabbit.activity_tracker import event_model
from whiterabbit.activity_tracker.db_interface import MemoryDBInterface
from whiterabbit.activity_tracker.db_interface import TinyDBInterface


class TestMemoryDBInterface(unittest.TestCase):
    def setUp(self):
        self.db = MemoryDBInterface()

    def test_insert(self):
        event = event_model.Event('foo')
        self.db.insert(event)

        events = self.db.filter({})
        self.assertEqual(1, len(events))
        for db_event in events:
            self.assertEqual(event, db_event)

    def test_delete(self):
        event = event_model.Event('foo')
        self.db.insert(event)
        self.db.delete(event)

        events = self.db.filter({})
        self.assertEqual(0, len(events))

    def test_update(self):
        event = event_model.Event('foo')
        self.db.insert(event)
        event.status = 'stop'
        self.db.update(event)

        events = self.db.filter({})
        self.assertEqual(1, len(events))
        for db_event in events:
            self.assertEqual(event, db_event)

    def test_filter_name(self):
        event_foo = event_model.Event('foo')
        self.db.insert(event_foo)
        event_bar = event_model.Event('bar')
        self.db.insert(event_bar)

        events = self.db.filter({'name': 'foo'})
        self.assertEqual(1, len(events))
        self.assertEqual(event_foo, events[0])

        events = self.db.filter({'name': 'bar'})
        self.assertEqual(1, len(events))
        self.assertEqual(event_bar, events[0])

    def test_filter_id(self):
        event_foo1 = event_model.Event('foo')
        self.db.insert(event_foo1)
        event_foo2 = event_model.Event('foo')
        self.db.insert(event_foo2)

        events = self.db.filter({'_id': event_foo1._id})
        #import ipdb; ipdb.set_trace()
        self.assertEqual(1, len(events))
        self.assertEqual(event_foo1, events[0])

        events = self.db.filter({'_id': event_foo2._id})
        self.assertEqual(1, len(events))
        self.assertEqual(event_foo2, events[0])


class TestTinyDBInterface(TestMemoryDBInterface):
    def setUp(self):
        self.db = TinyDBInterface(tempfile.mktemp())


if __name__ == '__main__':
    unittest.main()
