import unittest

import whiterabbit.activity_tracker.event_model as event_model
from whiterabbit.activity_tracker.event_manager import EventManager


class MockDB():
    def __init__(self):
        self._events = []

    def insert(self, event):
        self._events.append(event)

    def delete(self, event):
        for index, evnt in enumerate(self._events):
            if evnt._id == event._id:
                self._events.pop(index)

    def update(self, event):
        for index, evnt in enumerate(self._events):
            if evnt._id == event._id:
                self._events.pop(index)
                self._events.append(event)
                break

    def filter(self, event_filter):
        events = self._events
        for key, value in event_filter.items():
            filtered_events = []
            for event in events:
                if getattr(event, key) == value:
                    filtered_events.append(event)
            events = filtered_events
        return events


class TestEventManager(unittest.TestCase):
    def setUp(self):
        self.db = MockDB()
        self.em = EventManager(self.db)

    def test_start(self):
        self.em.start()
        self.assertEqual(len(self.db._events), 1)
        self.assertTrue(isinstance(self.db._events[0], event_model.Event))
        self.assertEqual(self.db._events[0].name, 'None')

    def test_start_with_name(self):
        self.em.start('foo')
        self.assertEqual(self.db._events[0].name, 'foo')

    def test_start_rv(self):
        event = self.em.start('foo')
        events = self.em.list({})
        self.assertEqual(events[0], event)

    def test_list(self):
        event1 = event_model.Event('foo')
        event1.status = 'stop'
        event2 = event_model.Event('foo')
        event2.status = 'start'
        event3 = event_model.Event('bar')
        event3.status = 'start'
        self.db._events.append(event1)
        self.db._events.append(event2)
        self.db._events.append(event3)

        events = self.em.list()
        self.assertEqual(3, len(events))
        for event in events:
            self.assertTrue(event._id in [event1._id, event2._id, event3._id])

    def test_list_filter_status(self):
        event1 = event_model.Event('foo')
        event2 = event_model.Event('bar')
        event1.status = 'stop'
        event2.status = 'start'
        self.db._events.append(event1)
        self.db._events.append(event2)
        stoped = self.em.list({'status': 'stop'})
        started = self.em.list({'status': 'start'})
        self.assertEqual(1, len(stoped))
        self.assertEqual(stoped[0]._id, event1._id)
        self.assertEqual(1, len(started))
        self.assertEqual(started[0]._id, event2._id)

    def test_list_filter_name(self):
        event1 = event_model.Event('foo')
        event1.status = 'stop'
        event2 = event_model.Event('foo')
        event2.status = 'start'
        event3 = event_model.Event('bar')
        event3.status = 'start'
        self.db._events.append(event1)
        self.db._events.append(event2)
        self.db._events.append(event3)

        foo_events = self.em.list({'name': 'foo'})
        self.assertEqual(2, len(foo_events))
        for event in foo_events:
            self.assertTrue(event._id in [event1._id, event2._id])

        bar_events = self.em.list({'name': 'bar'})
        self.assertEqual(1, len(bar_events))
        for event in bar_events:
            self.assertTrue(event._id in [event3._id])

    def test_stop_event_status(self):
        event = event_model.Event('foo')
        event.status = 'start'
        self.db._events.append(event)
        self.em.stop()
        self.assertEqual(len(self.em.list({'status': 'start'})), 0)

    def test_stop_event_stop_time_not_none(self):
        event = event_model.Event('foo')
        event.status = 'start'
        self.db._events.append(event)
        event_stoped = self.em.stop()
        self.assertNotEqual(event_stoped.stop_time, None)

    def test_delete_event(self):
        event = event_model.Event('foo')
        self.db._events.append(event)
        self.em.delete(event)
        self.assertEqual(len(self.em.list({'name': 'foo'})), 0)

    def test_backup(self):
        event1 = event_model.Event('foo')
        event1.status = 'stop'
        event2 = event_model.Event('foo')
        event2.status = 'stop'
        event3 = event_model.Event('bar')
        event3.status = 'start'
        self.db._events.append(event1)
        self.db._events.append(event2)
        self.db._events.append(event3)

        db_backup = MockDB()
        self.em.backup(db_backup)

        for event in db_backup._events:
            self.assertTrue(event._id in [event1._id, event2._id, event3._id])
