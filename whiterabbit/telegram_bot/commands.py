import functools


def _need_arguments(num_arguments):
    def decorator_need_arguments(func):
        @functools.wraps(func)
        def wrapper_need_arguments(*args, **kwargs):
            update = args[0]
            context = args[1]
            if not len(context.args) == num_arguments:
                update.message.reply_text('Number of arguments incorrect. See /help.')
            else:
                return func(*args, **kwargs)
        return wrapper_need_arguments
    return decorator_need_arguments


def _event_exists(func):
    @functools.wraps(func)
    def wrapper_event_exists(*args, **kwargs):
        update = args[0]
        context = args[1]
        event_name = context.args[0]
        if not event_name in []:
            update.message.reply_text('You must provide an existing event name. Use /list_names.')
        else:
            return func(*args, **kwargs)
    return wrapper_event_exists


def _list_names():
    return []


def list_names(update, context):
    """ List all event names."""
    events = _list_names()
    rply_message = 'Event list:'
    for event in events:
        rply_message += '\n {}'.format(event)
    update.message.reply_text(rply_message)


@_need_arguments(1)
def init(update, context):
    """ Initialize an event whit 'name'."""
    event_name = context.args[0]
    rply_message = 'Event {} started.'.format(event_name)
    update.message.reply_text(rply_message)


@_need_arguments(1)
@_event_exists
def stop(update, context):
    """ Stop the already initialized event whit 'name'."""
    event_name = context.args[0]
    rply_message = 'Event {} stoped.'.format(event_name)
    update.message.reply_text(rply_message)


@_need_arguments(1)
@_event_exists
def status(update, context):
    """ Print all the events for 'name'."""
    event_name = context.args[0]
    rply_message = 'Event {} status.'.format(event_name)
    update.message.reply_text(rply_message)
