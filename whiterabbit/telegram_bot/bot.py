import logging

from telegram.ext import Updater, CommandHandler

import whiterabbit.telegram_bot.commands as commands


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def error(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def start(update, context):
    help_message = 'This is the White Rabbit timetracker bot. Commands:'

    for command in commands.__dir__():
        if command[0] == '_':
            continue
        help_message += '\n /{command}: {help}'.format(
                                command=command,
                                help=getattr(commands, command).__doc__)

    update.message.reply_text(help_message)


def run_bot(token):
    updater = Updater(token, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('help', start))

    for command in commands.__dir__():
        if command[0] == '_':
            continue
        dp.add_handler(CommandHandler(command, getattr(commands, command),
                       pass_args=True, pass_chat_data=True))

    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    TOKEN="1064890066:AAErBomo-QPvLH_7nDmuDo2TAZ2iplGJcw0"
    run_bot(TOKEN)

