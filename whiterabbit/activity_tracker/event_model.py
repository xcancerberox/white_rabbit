import bson
import datetime


STATUS_STARTED = 'start'
STATUS_STOPED = 'stop'


def to_isoformat_if_not_none(date):
    if date is not None:
        return datetime.datetime.isoformat(date)
    return date


def from_isoformat_if_not_none(date):
    if date is not None:
        return datetime.datetime.fromisoformat(date)
    return date


class Event():
    def __init__(self, name, start_time=datetime.datetime.now(),
                 stop_time=None, status=STATUS_STARTED, _id=None):
        if not isinstance(name, str):
            raise TypeError

        self.name = name
        self.start_time = start_time
        self.stop_time = stop_time
        self.status = status

        if _id is None:
            _id = bson.objectid.ObjectId()

        self._id = _id

    @classmethod
    def loadd(cls, data):
        if not isinstance(data, dict):
            raise ValueError
        return cls(**data)

    def dumpd(self):
        return {'name': self.name, 'start_time': self.start_time,
                'stop_time': self.stop_time, 'status': self.status,
                '_id': self._id}

    def stop(self):
        self.status = STATUS_STOPED
        self.stop_time = datetime.datetime.now()

    def __repr__(self):
        return '{} -{}ed- {} ({}, {})'.format(self._id, self.status, self.name,
                                              self.start_time, self.stop_time)

    def __eq__(self, other):
        if self._id != other._id:
            eq = False
        elif self.name != other.name:
            eq = False
        elif self.status != other.status:
            eq = False
        elif self.start_time != other.start_time:
            eq = False
        elif self.stop_time != other.stop_time:
            eq = False
        else:
            eq = True
        return eq

    def __neq__(self, other):
        return not self.__eq__(other)
