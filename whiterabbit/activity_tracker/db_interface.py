import abc
import bson
import datetime
import pymongo
import tinydb


from whiterabbit.activity_tracker import event_model


def to_isoformat_if_not_none(date):
    if date is not None:
        return datetime.datetime.isoformat(date)
    return date


def from_isoformat_if_not_none(date):
    if date is not None:
        return datetime.datetime.fromisoformat(date)
    return date


class DBInterface(abc.ABC):
    def __init__(self):
        super().__init__()

    @abc.abstractmethod
    def insert(self, event):
        pass

    @abc.abstractmethod
    def delete(self, event):
        pass

    @abc.abstractmethod
    def update(self, event):
        pass

    @abc.abstractmethod
    def filter(self, event_filter):
        pass


class MongoDBInterface(DBInterface):
    query_started = {'status': 'start'}

    def __init__(self, addr, port):
        super().__init__()
        client = pymongo.MongoClient(addr, port)
        db = client.cancerbero
        self.events_collection = db.events

    def insert(self, event):
        self.events_collection.insert_one(event.dumpd())

    def delete(self, event):
        query = {'_id': event._id}
        self.events_collection.delete_one(query)

    def update(self, event):
        query = {'_id': event._id}
        self.events_collection.update_one(query, {'$set': event.dumpd()})

    def filter(self, event_filter):
        cursor = self.events_collection.find(event_filter)
        events = []
        for element in cursor:
            events.append(event_model.Event.loadd(element))
        return events


class TinyDBInterface(DBInterface):
    db_to_event = {
            '_id': bson.objectid.ObjectId,
            'start_time': from_isoformat_if_not_none,
            'stop_time': from_isoformat_if_not_none
            }
    event_to_db = {
            '_id': str,
            'start_time': to_isoformat_if_not_none,
            'stop_time': to_isoformat_if_not_none,
            }

    def __init__(self, filename):
        super().__init__()
        self.db = tinydb.TinyDB(filename)

    def convert_event_to_db(self, event):
        event_dict = event.dumpd()
        for key, value in self.event_to_db.items():
            event_dict[key] = value(getattr(event, key))
        return event_dict

    def convert_db_to_event(self, event_dict):
        for key, value in self.db_to_event.items():
            event_dict[key] = value(event_dict[key])

        return event_model.Event.loadd(event_dict)

    def insert(self, event):
        self.db.insert(self.convert_event_to_db(event))

    def delete(self, event):
        event = self.convert_event_to_db(event)
        query = tinydb.Query()
        matching_id_events = self.db.search(query._id == event['_id'])

        if len(matching_id_events) > 1:
            raise ValueError

        tinydb_id_to_remove = matching_id_events[0].doc_id
        self.db.remove(doc_ids=[tinydb_id_to_remove])

    def update(self, event):
        event = self.convert_event_to_db(event)
        query = tinydb.Query()

        self.db.update(event, query._id == event['_id'])

    def filter(self, event_filter):
        query = tinydb.Query()

        if event_filter == {}:
            docs = self.db.table().all()

        else:
            filter_key, filter_value = event_filter.popitem()
            if filter_key in self.event_to_db.keys():
                filter_value = self.event_to_db[filter_key](filter_value)

            queryimpl = (getattr(query, filter_key) == filter_value)

            for filter_key, filter_value in event_filter.items():
                queryimpl &= (getattr(query, filter_key) == filter_value)

            docs = self.db.search(queryimpl)

        return [self.convert_db_to_event(doc) for doc in docs]


class MemoryDBInterface(DBInterface):
    def __init__(self):
        self._events = []

    def insert(self, event):
        self._events.append(event)

    def delete(self, event):
        for index, evnt in enumerate(self._events):
            if evnt._id == event._id:
                self._events.pop(index)

    def update(self, event):
        for index, evnt in enumerate(self._events):
            if evnt._id == event._id:
                self._events.pop(index)
                self._events.append(event)
                break

    def filter(self, event_filter):
        events = self._events
        for key, value in event_filter.items():
            filtered_events = []
            for event in events:
                if getattr(event, key) == value:
                    filtered_events.append(event)
            events = filtered_events
        return events
