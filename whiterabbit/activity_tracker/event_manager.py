import whiterabbit.activity_tracker.event_model as event_model


class LocalNotEmptyError(Exception):
    pass


class EventNotUniqueError(Exception):
    pass


class EventNotFoundError(Exception):
    pass


class EventManager():
    def __init__(self, db):
        self.db = db

    def start(self, event_name=None):
        if event_name is None:
            event_name = 'None'
        event = event_model.Event(event_name)
        self.db.insert(event)
        return event

    def stop(self, event_id=None):
        events_filter = {'status': 'start'}
        if event_id is not None:
            events_filter['_id'] = event_id

        events_started = self.list(events_filter)

        if len(events_started) > 1:
            raise EventNotUniqueError

        if len(events_started) == 0:
            raise EventNotFoundError

        event = events_started[0]
        event.stop()
        self.db.update(event)
        return event

    def list(self, event_filter={}):
        return self.db.filter(event_filter)

    @property
    def events(self):
        for event in self.list():
            yield event

    def backup(self, db_bckp):
        for event in self.events:
            db_bckp.insert(event)

    def delete(self, event):
        self.db.delete(event)
