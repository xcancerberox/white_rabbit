# White Rabbit

*Oh dear! Oh dear! I shall be too late!*


## Development

You can start an virtualenv and install the package with pip

    white_rabbit $ virtualenv -p python3 venv
    white_rabbit $ . venv/bin/activate
    white_rabbit (venv)$ pip install -e .

We are using tox for runing tests

    $ tox
