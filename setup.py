# -*- encoding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup


setup(
    name='whiterabbit',
    version='0.1.0',
    license='MIT',
    description='',
    packages=find_packages(),
    install_requires=['bson',
                      'pymongo',
                      'tinydb',
                      'python-telegram-bot'],
)
